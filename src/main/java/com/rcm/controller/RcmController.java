package com.rcm.controller;

import com.rcm.domain.EOBDetailsModel;
import com.rcm.domain.SecurityDetails;
import com.rcm.entities.HealthCardDetails;
import com.rcm.entities.IdCardDetails;
import com.rcm.entities.PatientDetails;
import com.rcm.service.RcmService;
import com.rcm.util.ClaimResponse;
import com.rcm.util.Response;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(path = "/ocrApi")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "RCM",description = "Rcm application is for the automatic claim creation process")
public class RcmController {
    @Autowired
    RcmService service;
    /*
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/processTwoCards")
    public SecurityDetails processTwoCards(@RequestBody RequestPayload requestPayload) {
        return service.processTwoCards(requestPayload);
    }
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/processOneCard")
    public Object processOneCard(@RequestBody FilePayload filePayload) {
        return service.processOneCard(filePayload);
    }
    */
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/saveDetails")
    @Operation(summary = "Patient Registration api")
    public Response saveDetails(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Security details like IdCard and HealthCard related to Patient") @RequestBody SecurityDetails securityDetails) {
        return service.saveDetails(securityDetails);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @Operation(summary = "Get all the Patient information")
    @GetMapping(value = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SecurityDetails> getAll() throws Exception {
        return service.getAll();
    }
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @Operation(summary = "Update the Patient information with discharge and claim related details")
    @PostMapping(value = "/updatePatientDetails")
    public String updatePatientDetails(@io.swagger.v3.oas.annotations.parameters.RequestBody(description="Discharge summary and Claim related information",
            content = { @Content(mediaType = "application/json",examples = {@ExampleObject(value = "\n" +
                    "{\n" +
                    "            \"General_details\": {\n" +
                    "                \"Primary_insurance\": \"629929218034\",\n" +
                    "                \"Name\":\"IDTOP JELANI\", \n" +
                    "                \"Total Charge\" :\"100\",\n" +
                    "                \"App_Date\": \"25/12/2021\",\n" +
                    "                \"Time\": \"09:15 Pm\",\n" +
                    "                \"Ticket\": \"234567890\",\n" +
                    "                \"Acct\": \"0987654321\",\n" +
                    "                \"today_charge\": \"50\",\n" +
                    "                \"today_payment\": \"45\",\n" +
                    "                \"today_balance\": \"5\",\n" +
                    "                \"patient_id\": \"PID1234\",\n" +
                    "                \"symptom_1\": \"\",\n" +
                    "                \"symptom_2\": \"\",\n" +
                    "                \"symptom_3\": \"\",\n" +
                    "                \"symptom_4\": \"\",\n" +
                    "                \"symptom_5\": \"\",\n" +
                    "                \"superbill_name\":\"test\"\n" +
                    "            },\n" +
                    "            \"selected_values\": {\n" +
                    "        \"A150\": [\"32906\", \"32960\"],\n" +
                    "        \"A0109\": [\"36450\", \"36455\", \"81247\"]\n" +
                    "    }\n" +
                    "}\n")}
            ) })@RequestBody String jsonString) throws Exception {
        return service.updatePatientDetails(jsonString);
    }
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(value = "/verify")
    @Operation(summary = "Verify the super bill information")
    public String verify(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Verified super bill information",
            content = { @Content(mediaType = "application/json",examples = {@ExampleObject(value = "{\n" +
                    "\"General_details\":{\n" +
                    "\"patient_id\":\"PID9937\",\n" +
                    "\"Primary_insurance\":\"629929219152\",\n" +
                    "\"Name\":\"WALTER ELIAS DISNEY\",\n" +
                    "\"App_Date\":\"23/1/2021\",\n" +
                    "\"Time\":\"09:12 Pm\",\n" +
                    "\"Ticket\":\"01234\",\n" +
                    "\"Acct\":\"02346578\",\n" +
                    "\"today_charge\":\"50\",\n" +
                    "\"today_payment\":\"45\",\n" +
                    "\"today_balance\":\"5\",\n" +
                    "\"symptom_1\":\"fatigue\",\n" +
                    "\"symptom_2\":\"weight_loss\",\n" +
                    "\"symptom_3\":\"restlessness\",\n" +
                    "\"symptom_4\":\"lethargy\",\n" +
                    "\"symptom_5\":\"irregular_sugar_level\"\n" +
                    "},\n" +
                    "\"selected_values\":{\n" +
                    "\"E0822\":[\n" +
                    "\"36450\"\n" +
                    "]\n" +
                    "}\n" +
                    "}")}
            ) }) @RequestBody String jsonString) throws Exception {
        return service.verify(jsonString);
    }
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @Operation(summary = "Get all the Patients Checked In")
    @GetMapping(value = "/getAllPatients", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PatientDetails> getAllPatients() throws Exception {
        return service.getAllPatients();
    }
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @Operation(summary = "Get the IDCard information of the Patient")
    @GetMapping(value = "/getIdCard/{idCard}", produces = MediaType.APPLICATION_JSON_VALUE)
    public IdCardDetails getIdCard(@Parameter(description = "IdCard Number") @PathVariable Integer idCard) throws Exception {
        return service.getIdCard(idCard);
    }
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @Operation(summary = "Get the HealthCard information of the Patient ")
    @GetMapping(value = "/getHealthCard/{idCard}", produces = MediaType.APPLICATION_JSON_VALUE)
    public HealthCardDetails getHealthCard(@Parameter(description = "IdCard Number")@PathVariable Integer idCard) throws Exception {
        return service.getHealthCard(idCard);
    }
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @Operation(summary = "It Will create claim")
    @PostMapping(value = "/createClaim", produces = MediaType.APPLICATION_JSON_VALUE)
    public ClaimResponse createClaim(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Claim information",
            content = { @Content(mediaType = "application/json",examples = {@ExampleObject(value = "{\"PatientId\":\"Patient Id\",\"patientAccountNumber\":\"Patient Account number\"}")}
            ) }) @RequestBody String jsonString) throws Exception {
        return service.createClaim(jsonString);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @Operation(summary = "It will create EOB")
    @PostMapping(value = "/saveEob", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response saveEOBDetails(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "EOB information",
            content = { @Content(mediaType = "application/json",examples = {@ExampleObject(value = "{\n" +
                    "\t\"patientId\": \"PID111\",\n" +
                    "\t\"patientName\": \"ABC\",\n" +
                    "\t\"claimId\": \"CLID111\",\n" +
                    "\t\"providerId\": \"providerid\",\n" +
                    "\t\"providerName\": \"nameOfprovider\",\n" +
                    "\t\"eOBServiceDetailsList\": [\n" +
                    "\t\t\t {\t\t\n" +
                    "\t\t\t\t\"checkEFT\": \"111\",\n" +
                    "\t\t\t\t\"servDate\": \"2022-03-07T23:26:56\",\n" +
                    "\t\t\t\t\"billed\": \"11.22\",\n" +
                    "\t\t\t\t\"allowed\": \"10\",\n" +
                    "\t\t\t\t\"proCode\": \"12333\",\n" +
                    "\t\t\t\t\"deduct\": \"1\",\n" +
                    "\t\t\t\t\"coins\": \"abcdd\",\n" +
                    "\t\t\t\t\"provPD\": \"10\",\n" +
                    "\t\t\t\t\"ptResp\": \"1\",\n" +
                    "\t\t\t\t\"pos\": \"1\",\n" +
                    "\t\t\t\t\"nos\": \"1\",\n" +
                    "\t\t\t\t\"reasonCode\": \"test\",\n" +
                    "\t\t\t\t\"asgY\": \"test\"\n" +
                    "\t\t\t}\n" +
                    "\t\t,\n" +
                    "\t\t\n" +
                    "\t\t\t{\t\t\n" +
                    "\t\t\t\t\"checkEFT\": \"1778\",\n" +
                    "\t\t\t\t\"servDate\": \"2022-03-07T23:26:56\",\n" +
                    "\t\t\t\t\"billed\": \"51.22\",\n" +
                    "\t\t\t\t\"allowed\": \"30\",\n" +
                    "\t\t\t\t\"proCode\": \"12333\",\n" +
                    "\t\t\t\t\"deduct\": \"20\",\n" +
                    "\t\t\t\t\"coins\": \"abcdd\",\n" +
                    "\t\t\t\t\"provPD\": \"00\",\n" +
                    "\t\t\t\t\"ptResp\": \"20\",\n" +
                    "\t\t\t\t\"pos\": \"1\",\n" +
                    "\t\t\t\t\"nos\": \"1\",\n" +
                    "\t\t\t\t\"reasonCode\": \"test\",\n" +
                    "\t\t\t\t\"asgY\": \"test\"\n" +
                    "\t\t\t}\n" +
                    "\t\t\n" +
                    "\t]\n" +
                    "}")}
            ) }) @RequestBody String jsonString) throws Exception {
        return service.saveEOBDetails(jsonString);
    }


    /**
     * Get All EOB Details
     * @return
     * @throws Exception
     */
    /*
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @Operation(summary = "Get all the EOB related information")
    @GetMapping(value = "/getAllEOBDetails", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EOBDetailsModel> getAllEOBDetails() throws Exception {
        return service.getAllEOBDetails();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @Operation(summary = "Payment posting for the claim")
    @PostMapping(value = "/postPayUpdate", produces = MediaType.APPLICATION_JSON_VALUE)
    public String doPayment(@Parameter(description = "Claim Id") @RequestParam String claimId) throws Exception {
        return service.doPayment(claimId);
    }
    */
    
    /*@CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(value = "/uploadEob", produces = MediaType.APPLICATION_JSON_VALUE)
    public String uploadEob(@RequestBody String jsonString) throws Exception {
        return service.uploadEob(jsonString);
    }*/

}

