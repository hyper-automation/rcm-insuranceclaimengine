package com.rcm.repositories;

import com.rcm.entities.EOBDetails;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EOBDetailsRepository extends JpaRepository<EOBDetails,Integer> {
	List<EOBDetails> findByClaimId(String claimId);
	List<EOBDetails> findByEobFilePath(String filePath);
}
