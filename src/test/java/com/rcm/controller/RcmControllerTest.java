package com.rcm.controller;

import com.rcm.RcmInsuranceClaimEngineApplicationTest;
import com.rcm.domain.SecurityDetails;
import com.rcm.entities.HealthCardDetails;
import com.rcm.entities.IdCardDetails;
import com.rcm.entities.PatientDetails;
import com.rcm.util.ClaimResponse;
import com.rcm.util.Response;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class RcmControllerTest extends RcmInsuranceClaimEngineApplicationTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    RcmController rcmController;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void saveDetailsTest() throws Exception {
        SecurityDetails securityDetails = new SecurityDetails();
        IdCardDetails idCardDetails = new IdCardDetails();
        idCardDetails.setCard_type("driving");
        idCardDetails.setAddress("USA");
        idCardDetails.setState("NZ");
        idCardDetails.setCountry("USA");
        idCardDetails.setDob("07/07/94");
        idCardDetails.setFirst_name("Smith James");
        idCardDetails.setLast_name("James");
        idCardDetails.setGender("male");
        idCardDetails.setWeight("80");
        idCardDetails.setId_number("D12345678");
        idCardDetails.setHeight("5.7");
        idCardDetails.setEye_color("blue");
        idCardDetails.setSuperBillName("walter");
        HealthCardDetails healthCardDetails = new HealthCardDetails();
        healthCardDetails.setInsurance_provider("Anthem");
        healthCardDetails.setInsurance_plan("BPPO");
        healthCardDetails.setFirst_name("Smith James");
        healthCardDetails.setLast_name("James");
        healthCardDetails.setId_number("11111111");
        healthCardDetails.setRxBIN("58477");
        healthCardDetails.setGroup_id("HRNYBT");
        healthCardDetails.setCard_issued_date(new Date("1/1/21"));
        securityDetails.setId_card(idCardDetails);
        securityDetails.setHealth_card(healthCardDetails);
        Response response = rcmController.saveDetails(securityDetails);
        Assert.assertEquals("Details are saved", response.getMessage());
        Assert.assertEquals(200, response.getStatus());

    }

    @Test
    void getAllTest() throws Exception {
        List<SecurityDetails> securityDetailsList = rcmController.getAll();
        Assert.assertEquals(true, securityDetailsList.size() >= 0);

    }

    @Test
    void updatePatientDetailsTest() throws Exception {
        String jsonString = "\n" +
                "{\n" +
                "            \"General_details\": {\n" +
                "                \"Primary_insurance\": \"629929218034\",\n" +
                "                \"Name\":\"IDTOP JELANI\", \n" +
                "                \"Total Charge\" :\"100\",\n" +
                "                \"App_Date\": \"25/12/2021\",\n" +
                "                \"Time\": \"09:15 Pm\",\n" +
                "                \"Ticket\": \"234567890\",\n" +
                "                \"Acct\": \"0987654321\",\n" +
                "                \"today_charge\": \"50\",\n" +
                "                \"superbill_name\":\"simth_bill\",\n" +
                "                \"today_payment\": \"45\",\n" +
                "                \"today_balance\": \"5\",\n" +
                "                \"patient_id\": \"PID1234\",\n" +
                "                \"symptom_1\": \"\",\n" +
                "                \"symptom_2\": \"\",\n" +
                "                \"symptom_3\": \"\",\n" +
                "                \"symptom_4\": \"\",\n" +
                "                \"symptom_5\": \"\"\n" +
                "            },\n" +
                "            \"selected_values\": {\n" +
                "        \"A150\": [\"32906\", \"32960\"],\n" +
                "        \"A0109\": [\"36450\", \"36455\", \"81247\"]\n" +
                "    }\n" +
                "}\n";
        String response = rcmController.updatePatientDetails(jsonString);
        Assert.assertEquals("PID1234", response);


    }

    @Test
    void verifyTest() throws Exception {
        String jsonString = "\n" +
                "{\n" +
                "            \"General_details\": {\n" +
                "                \"Primary_insurance\": \"629929218034\",\n" +
                "                \"Name\":\"IDTOP JELANI\", \n" +
                "                \"Total Charge\" :\"100\",\n" +
                "                \"App_Date\": \"25/12/2021\",\n" +
                "                \"Time\": \"09:15 Pm\",\n" +
                "                \"Ticket\": \"234567890\",\n" +
                "                \"Acct\": \"0987654321\",\n" +
                "                \"today_charge\": \"50\",\n" +
                "                \"today_payment\": \"45\",\n" +
                "                \"today_balance\": \"5\",\n" +
                "                \"patient_id\": \"PID1234\",\n" +
                "                \"symptom_1\": \"\",\n" +
                "                \"symptom_2\": \"\",\n" +
                "                \"symptom_3\": \"\",\n" +
                "                \"symptom_4\": \"\",\n" +
                "                \"symptom_5\": \"\"\n" +
                "            },\n" +
                "            \"selected_values\": {\n" +
                "        \"A150\": [\"32906\", \"32960\"],\n" +
                "        \"A0109\": [\"36450\", \"36455\", \"81247\"]\n" +
                "    }\n" +
                "}\n";
        String response = rcmController.verify(jsonString);
        Assert.assertEquals("PID1234", response);
    }

    @Test
    void getAllPatientsTest() throws Exception {
        List<PatientDetails> patientDetailsList = rcmController.getAllPatients();


        Assert.assertEquals(true, patientDetailsList.size() >= 0);
    }

    @Test
    void getIdCardTest() throws Exception {
        IdCardDetails idCardDetails = rcmController.getIdCard(39);

        Assert.assertEquals(true, idCardDetails.getFirst_name().equalsIgnoreCase("Johnson John"));
    }

    @Test
    void getHealthCardTest() throws Exception {
        HealthCardDetails healthCardDetails = rcmController.getHealthCard(39);

        Assert.assertEquals(true, healthCardDetails.getFirst_name().equalsIgnoreCase("Johnson John"));
    }

    @Test
    void createClaimTest() throws Exception {
        String jsonString = "{\n" +
                "\"PatientId\":\"PID1234\",\n" +
                "\"patientAccountNumber\":\"0234118\"\n" +
                "}";
        ClaimResponse response = rcmController.createClaim(jsonString);

        Assert.assertEquals("Claim Created", response.getMessage());
        Assert.assertEquals(false, response.getClaimId().isEmpty());

    }


}