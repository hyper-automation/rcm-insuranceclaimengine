package com.rcm.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class RequestPayload {
    private String file_type_1;
    private String base64_id_card;
    private String file_type_2;
    private String base64_health_card;
}
