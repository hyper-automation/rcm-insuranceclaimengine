package com.rcm.service.module;

import com.rcm.domain.EOBDetailsModel;
import com.rcm.util.Response;

import java.util.List;

public interface EOBServiceDetailsService {
    public void save(String jsonString, int eobId, Response response);
    public void getAllEobServiceList(Integer eobId,EOBDetailsModel eobDetailsModel);
}
