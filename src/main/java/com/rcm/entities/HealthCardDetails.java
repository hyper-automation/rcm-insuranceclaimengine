package com.rcm.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
@Data
@Table(name="HealthCardDetails")
public class HealthCardDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String insurance_provider;
    @Column
    private String insurance_plan;
    @Column
    private String first_name;
    @Column
    private String last_name;
    @Column(name="id_number")
    private String  id_number;
    @Column
    private String rxBIN;
    @Column
    private String group_id;
    @Column
    private Date card_issued_date;
    @Column
    private Integer idCard;

}
