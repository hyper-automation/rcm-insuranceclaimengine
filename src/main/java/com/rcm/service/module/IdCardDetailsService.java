package com.rcm.service.module;

import com.rcm.domain.SecurityDetails;
import com.rcm.entities.IdCardDetails;
import com.rcm.util.Response;

import java.awt.geom.RectangularShape;
import java.util.List;

public interface IdCardDetailsService {
    void save(SecurityDetails securityDetails, Response response);
    void getAllIdcards(List<SecurityDetails> securityDetailsList);
    IdCardDetails getIdCard(Integer idCard);
    List<IdCardDetails> getIdCardDetails(List<Integer> ids);
}
