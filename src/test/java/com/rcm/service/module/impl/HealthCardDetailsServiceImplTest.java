package com.rcm.service.module.impl;

import com.rcm.domain.SecurityDetails;
import com.rcm.entities.HealthCardDetails;
import com.rcm.util.Response;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

class HealthCardDetailsServiceImplTest {
    HealthCardDetailsServiceImpl healthCardDetailsService;

    @Test
    void save() {
        healthCardDetailsService = new HealthCardDetailsServiceImpl();
        Response response = new Response();
        healthCardDetailsService.save(new SecurityDetails(), null, null, null, "test", response);
        Assert.assertEquals("Health Card Details not saved", response.getMessage());
    }

    @Test
    void getByIdcard() {
        healthCardDetailsService = new HealthCardDetailsServiceImpl();
        SecurityDetails securityDetails = new SecurityDetails();
        healthCardDetailsService.getByIdcard(securityDetails, 1);
        Assert.assertEquals(null,securityDetails.getHealth_card());
    }


}