package com.rcm.service.module.impl;

import com.rcm.RcmInsuranceClaimEngineApplicationTest;
import com.rcm.util.Response;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class PatientDetailsServiceImplTest extends RcmInsuranceClaimEngineApplicationTest {
    PatientDetailsServiceImpl patientDetailsService;

    @Test
    void save() {
        patientDetailsService=new PatientDetailsServiceImpl();
        Response response=new Response();
        patientDetailsService.save(1,null,null,"test",response,"test");
      Assert.assertEquals("Patient Details are not saved",response.getMessage());
    }

    @Test
    void update() {
        patientDetailsService=new PatientDetailsServiceImpl();
        Response response=new Response();
        String jsonString = "\n" +
                "{\n" +
                "            \"General_details\": {\n" +

                "                \"symptom_1\": \"\",\n" +
                "                \"symptom_2\": \"\",\n" +
                "                \"symptom_3\": \"\",\n" +
                "                \"symptom_4\": \"\",\n" +
                "                \"symptom_5\": \"\"\n" +
                "            },\n" +
                "            \"selected_values\": {\n" +
                "        \"A150\": [\"32906\", \"32960\"],\n" +
                "        \"A0109\": [\"36450\", \"36455\", \"81247\"]\n" +
                "    }\n" +
                "}\n";
        patientDetailsService.update(jsonString,response);
        Assert.assertEquals("Patient is not checkedIn",response.getMessage());

    }

    @Test
    void updateVerify() {
        patientDetailsService=new PatientDetailsServiceImpl();
        Response response=new Response();
        String jsonString = "\n" +
                "{\n" +
                "            \"General_details\": {\n" +
                "                \"symptom_1\": \"\",\n" +
                "                \"symptom_2\": \"\",\n" +
                "                \"symptom_3\": \"\",\n" +
                "                \"symptom_4\": \"\",\n" +
                "                \"symptom_5\": \"\"\n" +
                "            },\n" +
                "            \"selected_values\": {\n" +
                "        \"A150\": [\"32906\", \"32960\"],\n" +
                "        \"A0109\": [\"36450\", \"36455\", \"81247\"]\n" +
                "    }\n" +
                "}\n";
        patientDetailsService.updateVerify(jsonString,response);
        Assert.assertEquals("NotVerified",response.getMessage());
    }

}