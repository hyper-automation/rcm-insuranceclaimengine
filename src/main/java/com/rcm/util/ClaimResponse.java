package com.rcm.util;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClaimResponse {
    private String claimId;
    private String message;
}
