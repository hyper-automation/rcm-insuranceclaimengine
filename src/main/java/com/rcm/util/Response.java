package com.rcm.util;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
private String message;
private int Status;
}
