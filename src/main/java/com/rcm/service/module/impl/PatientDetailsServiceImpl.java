package com.rcm.service.module.impl;

import com.rcm.entities.HealthCardDetails;

import com.rcm.entities.PatientDetails;
import com.rcm.repositories.HealthCardRepository;
import com.rcm.repositories.PatientDetailsRepository;
import com.rcm.service.module.PatientDetailsService;
import com.rcm.util.ClaimResponse;
import com.rcm.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j

public class PatientDetailsServiceImpl implements PatientDetailsService {
    @Autowired
    PatientDetailsRepository patientDetailsRepository;
    @Autowired
    HealthCardRepository healthCardRepository;

    @Override
    public void save(Integer idCard, String pid,String name,String superbillName, Response response,String insurenceNumber) {
        try {
            PatientDetails patientDetails = new PatientDetails();
            patientDetails.setPatientId(pid);
            patientDetails.setIdCard(idCard);
            patientDetails.setName(name);
            patientDetails.setSuperBillName(superbillName);
            patientDetails.setModifiedOn(LocalDateTime.now());
            patientDetails.setModifiedBy("jhon");
            patientDetails.setStatus("Active");
            patientDetails.setPrimaryInsurance(insurenceNumber);
       //     patientDetails.setEobFilePath("");
            patientDetailsRepository.save(patientDetails);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setMessage("Patient Details are not saved");
        }
    }

    @Override
    public void update(String jsonString, Response response) {
        try {
        	System.out.println(jsonString);
            JSONObject jsonObject = new JSONObject(jsonString);
        	JSONObject generalDetails = new JSONObject(jsonObject.get("General_details").toString());
            //JSONObject generalDetails = jsonObject.getJSONObject("General_details");
            String patientId = generalDetails.getString("patient_id");
            List<PatientDetails> patient_Details = patientDetailsRepository.findByPatientId(patientId);
            PatientDetails patientDetails=patient_Details.get(0);
            HealthCardDetails healthCardDetails=healthCardRepository.findByIdCard(patientDetails.getIdCard()).get();
            patientDetails.setPatientAccountNumber(generalDetails.get("Acct").toString());
            patientDetails.setName(generalDetails.getString("Name"));
            patientDetails.setApointmentDate(new SimpleDateFormat("yyyy-MM-dd").parse(generalDetails.get("App_Date").toString()));
            patientDetails.setTotalCharge(Integer.parseInt(generalDetails.get("today_charge").toString()));
            patientDetails.setTotalBalance(Integer.parseInt(generalDetails.get("today_balance").toString()));
            patientDetails.setTotalPayment(Integer.parseInt(generalDetails.get("today_payment").toString()));
            patientDetails.setJsonData(jsonString);
            patientDetails.setSuperBillName(generalDetails.getString("superbill_name"));
            patientDetails.setVerify(false);
            patientDetails.setPrimaryInsurance(healthCardDetails.getId_number());
            patientDetailsRepository.save(patientDetails);
            response.setMessage(patientId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setMessage("Patient is not checkedIn");
        }
    }

    @Override
    public void updateVerify(String jsonString, Response response) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONObject generalDetails = new JSONObject(jsonObject.get("General_details").toString());
            //JSONObject generalDetails = jsonObject.getJSONObject("General_details");
            String patientId = generalDetails.getString("patient_id");
            List<PatientDetails> patient_Details = patientDetailsRepository.findByPatientId(patientId);
            PatientDetails patientDetails=patient_Details.get(0);
            patientDetails.setVerify(true);
            patientDetails.setJsonData(jsonString);
            patientDetailsRepository.save(patientDetails);
            response.setMessage(patientId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setMessage("NotVerified");
        }
    }

    @Override
    public void getAllPatients(List<PatientDetails> patientDetailsList) {
        patientDetailsList.addAll(patientDetailsRepository.findByStatusOrderByModifiedOnDesc("Active"));
    }

    @Override
    public void createClaim(ClaimResponse response, String jsonString) {
        JSONObject jsonObject = new JSONObject(jsonString);
        String claimId = "CLID" + jsonObject.getString("patientAccountNumber");
        String patientId = jsonObject.getString("PatientId");
        PatientDetails patientDetails = patientDetailsRepository.findById(patientId).orElse(new PatientDetails());
        patientDetails.setClaimId(claimId);
        patientDetailsRepository.save(patientDetails);
        response.setClaimId(claimId);
        response.setMessage("Claim Created");
    }
/*
    @Override
    public void getByIdcard(SecurityDetails securityDetails, Integer id) {
        try {
            PatientDetails patientDetails = patientDetailsRepository.findByIdCard(id).orElse(new PatientDetails());
            } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

 */
    @Override
    public List<PatientDetails> getPatientDetails(String patientId){
    	return patientDetailsRepository.findByPatientId(patientId);
    }
    
    @Override
    public void updatePayStatus(String claimId, Response response) {
        try {
        	log.info("text"+ claimId);
            //PatientDetails patientDetails = patientDetailsRepository.findByClaimId(claimId);
        	List<PatientDetails> patientDetailsList = patientDetailsRepository.findByClaimId(claimId);
        	//System.out.println(patientDetailsList);
        	PatientDetails patientDetails=patientDetailsList.get(0);
        	patientDetails.setPayStatus("true");
            patientDetailsRepository.save(patientDetails);
            response.setMessage("true");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setMessage("false");
        }
    }
    
    /*@Override
    public String updatefilePath(String filename,String pateintId, Response response) {
    	String server = "hamiddleware.centralindia.cloudapp.azure.com";
    	String user = "newftpuser";
    	String password = "^&XW8ARccie76";
    	int port = 21;
    	FTPClient ftpClient = new FTPClient();
    	String filePath = "/home/evoluteadmin/rcm_eob" + "/"+ filename ;
    	FileInputStream fis = null;
    	try {
    		ftpClient.connect(server, port);
            ftpClient.login(user, password);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            //OutputStream outputStream = ftpClient.storeFileStream(filePath);
            //byte[] bytesIn = stringBuffer.toString().getBytes(StandardCharsets.UTF_8);
            //outputStream.write(bytesIn);
            //outputStream.close();
            fis = new FileInputStream(filename);
            ftpClient.storeFile(filePath, fis);
            fis.close();
            boolean completed = ftpClient.completePendingCommand();
            if (completed) {
                System.out.println("File uploaded successfully");
                PatientDetails patientDetails = patientDetailsRepository.findByPatientId(pateintId).get(0);
                patientDetails.setEobFilePath(filename );
                patientDetailsRepository.save(patientDetails);
                response.setMessage("File Saved"+ filename);
                return (filename );
                }
            
        	} 
    	catch (Exception e) {
    		response.setMessage(e.getMessage());
            return e.getMessage();
        	}
        return "file not saved";
    }*/
}
