package com.rcm.domain;

import com.rcm.entities.HealthCardDetails;
import com.rcm.entities.IdCardDetails;
import com.rcm.entities.PatientDetails;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class SecurityDetails {
    IdCardDetails id_card;

   HealthCardDetails health_card;

}
