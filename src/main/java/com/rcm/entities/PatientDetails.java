package com.rcm.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "PatientDetails")
@Data
@Getter
@Setter
public class PatientDetails implements Serializable {
    @Id
    @Column
    private String patientId;
    @Column
    private String name;
    @Column
    private String patientAccountNumber;
    @Column
    private Date apointmentDate;
    @Column
    private String primaryInsurance;
    @Column
    private Integer totalCharge;
    @Column
    private Integer totalPayment;
    @Column
    private Integer totalBalance;
    @Column
    private String claimId;
    @Column
    private Integer idCard;
    @Column
    private String jsonData;
    @Column
    private Boolean verify;
    @Column
    private String superBillName;
    @Column
    private LocalDateTime modifiedOn;
    @Column
    private String modifiedBy;
    @Column
    private String status;
    @Column
    private String payStatus;
   /* @Column
    private String eobFilePath;*/
}
