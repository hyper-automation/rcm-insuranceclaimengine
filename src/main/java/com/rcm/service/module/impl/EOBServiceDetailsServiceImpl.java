package com.rcm.service.module.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rcm.domain.EOBDetailsModel;
import com.rcm.entities.EOBServiceDetails;
import com.rcm.repositories.EOBServiceDetailsRepository;
import com.rcm.service.module.EOBServiceDetailsService;
import com.rcm.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class EOBServiceDetailsServiceImpl implements EOBServiceDetailsService {
    @Autowired
    EOBServiceDetailsRepository eobServiceDetailsRepository;
    @Override
    public void save(String jsonString, int eobId, Response response) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            ObjectMapper mapper = new ObjectMapper();
            EOBServiceDetails eobServiceDetails = new EOBServiceDetails();
            String providerId = jsonObject.getString("provider_number");
            String providerName = "";
            		//jsonObject.getString("providerName");
            //for (int i = 0; i < eobserviceList.length(); i++) {
                //JSONObject json=eobserviceList.getJSONObject(i);
                //EOBServiceDetails eobServiceDetails=mapper.readValue(json.toString(),EOBServiceDetails.class);
                eobServiceDetails.setEobId(eobId);
                eobServiceDetails.setProviderId(providerId);
                eobServiceDetails.setProviderName(providerName);
                int a= Integer.parseInt(jsonObject.get("pos").toString());
                eobServiceDetails.setPos(a);
                int b= Integer.parseInt(jsonObject.get("nos").toString());
                eobServiceDetails.setNos(b);
                eobServiceDetails.setServDate(new SimpleDateFormat("yyyy-MM-dd").parse(jsonObject.get("serv date").toString()));
                eobServiceDetails.setProCode(jsonObject.get("proc").toString());
                eobServiceDetails.setAllowed(jsonObject.get("allowed amount").toString());
                eobServiceDetails.setBilled(jsonObject.get("billed amount").toString());
                eobServiceDetails.setDeduct(jsonObject.get("deduct").toString());
                eobServiceDetails.setCoins(jsonObject.get("coins").toString());
                eobServiceDetails.setAsgY(jsonObject.get("asg y").toString());
                eobServiceDetails.setProvPD(jsonObject.get("prov pd").toString());
                eobServiceDetails.setPtResp(jsonObject.get("pt resp").toString());
                eobServiceDetails.setCheckEFT(jsonObject.get("check").toString());
                //JSONArray eobserviceList = jsonObject.getJSONArray("eOBServiceDetailsList");
                eobServiceDetailsRepository.save(eobServiceDetails);
                
            
        }catch (Exception e){
            log.error(e.getMessage());
            response.setMessage("eob service details not saved");
            response.setStatus(400);
        }
    }

    @Override
    public void getAllEobServiceList(Integer eobId, EOBDetailsModel eobDetailsModel) {
        List<EOBServiceDetails> eobServiceDetailsList=eobServiceDetailsRepository.findByEobId(eobId);
        eobDetailsModel.setEOBServiceDetails(eobServiceDetailsList);
    }
}
