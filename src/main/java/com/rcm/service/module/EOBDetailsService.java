package com.rcm.service.module;

import com.rcm.domain.EOBDetailsModel;
import com.rcm.util.Response;
import com.rcm.entities.EOBDetails;

import java.util.List;

public interface EOBDetailsService {
    public void save(String jsonString, Response response);
    public void getAllEOBDetails(List<EOBDetailsModel> eobDetailsModelList);
    public EOBDetails getEobDetails(String filePath);
    public List<String> getAllFilenames();
}
