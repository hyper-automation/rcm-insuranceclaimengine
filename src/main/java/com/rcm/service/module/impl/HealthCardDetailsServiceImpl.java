package com.rcm.service.module.impl;

import com.rcm.domain.SecurityDetails;

import com.rcm.entities.HealthCardDetails;
import com.rcm.repositories.HealthCardRepository;
import com.rcm.service.module.HealthCardDetailsService;
import com.rcm.service.module.PatientDetailsService;
import com.rcm.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
@Slf4j
public class HealthCardDetailsServiceImpl implements HealthCardDetailsService {

    @Autowired
    HealthCardRepository healthCardRepository;
    @Autowired
    PatientDetailsService patientDetailsService;

    @Override
    public void save(SecurityDetails securityDetails, Integer idCard,String pid,String name, String superbillname, Response response) {
        try {
            HealthCardDetails healthCardDetails = securityDetails.getHealth_card();
            healthCardDetails.setIdCard(idCard);
            healthCardRepository.save(healthCardDetails);
            patientDetailsService.save(idCard,pid,name,superbillname,response,healthCardDetails.getId_number());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setMessage("Health Card Details not saved");
        }
    }

    @Override
    public void getByIdcard(SecurityDetails securityDetails, Integer id) {
        try {
            HealthCardDetails healthCardDetails = (healthCardRepository.findByIdCard(id).isPresent()) ? healthCardRepository.findByIdCard(id).get() : new HealthCardDetails();
            securityDetails.setHealth_card(healthCardDetails);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    @Override
    public HealthCardDetails getHealthCard(Integer idCard) {
        return healthCardRepository.findByIdCard(idCard).get();
    }
    

}
