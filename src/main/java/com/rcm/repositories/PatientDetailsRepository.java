package com.rcm.repositories;

import com.rcm.entities.PatientDetails;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PatientDetailsRepository extends JpaRepository<PatientDetails,String> {
    List<PatientDetails> findByStatusOrderByModifiedOnDesc(String status);
    List<PatientDetails> findByPatientId(String patientId);
    List<PatientDetails> findByClaimId(String claimId);
}
