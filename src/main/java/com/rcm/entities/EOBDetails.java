package com.rcm.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Data
@Table(name="EOBDetails")
public class EOBDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer eobId;
    @Column
    private String patientId;
    @Column
    private String claimId;
    @Column
    private String superBillId;
    @Column
    private String  providerId;
    @Column
    private String patientName;
    @Column
    private String eobFilePath;

}
