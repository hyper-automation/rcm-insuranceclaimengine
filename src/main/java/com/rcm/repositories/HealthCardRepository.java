package com.rcm.repositories;

import com.rcm.entities.HealthCardDetails;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HealthCardRepository extends JpaRepository<HealthCardDetails, Integer> {

    Optional<HealthCardDetails> findByIdCard(Integer idCard);
    
}


