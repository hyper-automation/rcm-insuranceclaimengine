package com.rcm.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
@Data
@Table(name="EOBServiceDetails")
public class EOBServiceDetails implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String providerId;
    @Column
    private String checkEFT;
    @Column
    private String providerName;
    @Column
    private Date servDate;
    @Column
    private Integer pos;
    @Column
    private Integer nos;
    @Column
    private String proCode;
    @Column
    private String allowed;
    @Column
    private String billed;
    @Column
    private String deduct;
    @Column
    private String coins;
    @Column
    private String reasonCode;
    @Column
    private String asgY;
    @Column
    private String provPD;
    @Column
    private String ptResp;
    @Column
    private Integer eobId;

}