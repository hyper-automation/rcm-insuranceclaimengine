package com.rcm.service.module.impl;

import com.rcm.domain.EOBDetailsModel;


import com.rcm.entities.EOBDetails;
import com.rcm.entities.PatientDetails;
import com.rcm.entities.HealthCardDetails;
import com.rcm.repositories.EOBDetailsRepository;
import com.rcm.repositories.HealthCardRepository;
import com.rcm.repositories.PatientDetailsRepository;
import com.rcm.service.module.EOBDetailsService;
import com.rcm.service.module.HealthCardDetailsService;
import com.rcm.service.module.EOBServiceDetailsService;
import com.rcm.service.module.PatientDetailsService;
import com.rcm.service.module.IdCardDetailsService;
import com.rcm.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.*;
import java.util.Optional;
@Service
@Slf4j
public class EOBDetailsServiceImpl implements EOBDetailsService {
    @Autowired
    PatientDetailsRepository patientDetailsRepository;
    @Autowired
    HealthCardRepository healthCardRepository;
    @Autowired
    EOBDetailsRepository eobDetailsRepository;
    @Autowired
    EOBServiceDetailsService eobServiceDetailsService;
    @Autowired
    HealthCardDetailsService healthCardDetailsService;
    @Autowired
    PatientDetailsService patientDetailsService;
    @Autowired
    IdCardDetailsService idCardDetailsService;


    @Override
    public void save(String jsonString, Response response) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            EOBDetails eobDetails = new EOBDetails();
            List<EOBDetails> eobDetailsList=eobDetailsRepository.findByClaimId(jsonObject.getString("claim id"));
            //String fPath = jsonObject.getString("filepath");
            //List<EOBDetails> eobDetailsList = eobDetailsRepository.findAll();
            //for(EOBDetails eob: eobs){
        		//filenames.add(eob.getEobFilePath());
        		//String filename = eob.getEobFilePath();
        		System.out.println(eobDetailsList);
         		if(!(eobDetailsList.isEmpty())) {
        			int id=0;
        			for(EOBDetails eob:eobDetailsList) {
        				id = eob.getEobId();
        			}
        		System.out.println(jsonString);
            	eobServiceDetailsService.save(jsonString, id ,response);
                if(response.getStatus()!=400) {
                    response.setMessage("Details are saved");
                    response.setStatus(200);
                //}
                }
                return;
            }
            String patientId = jsonObject.getString("member id");
            eobDetails.setPatientId(patientId);
            eobDetails.setClaimId(jsonObject.getString("claim id"));
            eobDetails.setPatientName(jsonObject.getString("patient name"));
            PatientDetails patientDetails = patientDetailsRepository.findById(patientId).orElse(new PatientDetails());
            eobDetails.setSuperBillId(patientDetails.getSuperBillName());
            eobDetails.setProviderId(jsonObject.getString("provider_number"));
            eobDetails.setEobFilePath(jsonObject.getString("filepath"));
            EOBDetails eobDetailsDB = eobDetailsRepository.save(eobDetails);
            eobServiceDetailsService.save(jsonString, eobDetailsDB.getEobId(),response);
            if(response.getStatus()!=400) {
                response.setMessage("Details are saved");
                response.setStatus(200);
            }

        }catch (Exception e){
            log.error(e.getMessage(), e);
            response.setMessage("EOBDetails are not saved");
            response.setStatus(400);
        }
    }

    @Override
    public void getAllEOBDetails(List<EOBDetailsModel> eobDetailsModelList) {
        List<EOBDetails> eobDetailsList=eobDetailsRepository.findAll();

        for (EOBDetails eobDetails:eobDetailsList   ) {
            EOBDetailsModel eobDetailsModel=new EOBDetailsModel();
            eobDetailsModel.setEobId(eobDetails.getEobId());
            eobDetailsModel.setClaimId(eobDetails.getClaimId());
            eobDetailsModel.setPatientId(eobDetails.getPatientId());
            eobDetailsModel.setPatientName(eobDetails.getPatientName());
            eobDetailsModel.setProviderId(eobDetails.getProviderId());
            eobDetailsModel.setSuperBillId(eobDetails.getSuperBillId());
            eobDetailsModel.setEobFilePath(eobDetails.getEobFilePath());
            eobServiceDetailsService.getAllEobServiceList(eobDetails.getEobId(), eobDetailsModel);
            PatientDetails patientDetails=patientDetailsRepository.findById(eobDetails.getPatientId()).orElse(new PatientDetails());
            HealthCardDetails healthCardDetail=healthCardRepository.findByIdCard(patientDetails.getIdCard()).get();
            List<HealthCardDetails> healthCardDetails = new ArrayList<>();
            healthCardDetails.add(healthCardDetail);
            eobDetailsModel.setHealthCardDetails(healthCardDetails);
            List<Integer> ids = new ArrayList<>();
            for (HealthCardDetails hcDetails:healthCardDetails) {
            	ids.add(hcDetails.getIdCard());
            }
            eobDetailsModel.setIdCardDetails(idCardDetailsService.getIdCardDetails(ids));
            eobDetailsModel.setPatientDetails(patientDetailsService.getPatientDetails(eobDetails.getPatientId()));
            eobDetailsModelList.add(eobDetailsModel);
        }

    }
    
    @Override
    public EOBDetails getEobDetails(String filePath) {
    	return eobDetailsRepository.findByEobFilePath(filePath).get(0);
    }
    
    @Override
    public List<String> getAllFilenames(){
    	List<String> filenames= new ArrayList<>();
    	List<EOBDetails> eobs= eobDetailsRepository.findAll();
    	for(EOBDetails eob: eobs){
    		//filenames.add(eob.getEobFilePath());
    		String filename = eob.getEobFilePath();
    		File f = new File(filename);
    		filenames.add(f.getName());
    	}
    	return filenames;
    }
    


}
