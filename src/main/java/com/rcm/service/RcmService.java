package com.rcm.service;

import com.rcm.domain.EOBDetailsModel;

import com.rcm.domain.SecurityDetails;
import com.rcm.entities.HealthCardDetails;
import com.rcm.entities.IdCardDetails;
import com.rcm.entities.PatientDetails;
import com.rcm.service.module.EOBDetailsService;
import com.rcm.util.ClaimResponse;
import com.rcm.util.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import java.io.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


@Service
public class RcmService {
    @Autowired
    RcmServiceHelper rcmServiceHelper;
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    EOBDetailsService eobDetailsService;
    
    /*
    public SecurityDetails processTwoCards(RequestPayload requestPayload){
       return rcmServiceHelper.processTwoCards(requestPayload);
    }
    public Object processOneCard(FilePayload filePayload){
        return rcmServiceHelper.processOneCard(filePayload);
    }*/
    public Response saveDetails(SecurityDetails securityDetails){
        return rcmServiceHelper.saveDetails(securityDetails);
    }
    public List<SecurityDetails> getAll(){
        return rcmServiceHelper.getAll();
    }
    public List<PatientDetails> getAllPatients(){
        return rcmServiceHelper.getAllPatients();
    }
    public String updatePatientDetails(String jsonString){return rcmServiceHelper.updatePatientDetails(jsonString);    }
    public String verify(String jsonString){return rcmServiceHelper.verify(jsonString);    }
    public ClaimResponse createClaim(String jsonString){return rcmServiceHelper.createClaim(jsonString);}
    public IdCardDetails getIdCard(Integer idCard){return rcmServiceHelper.getIdCard(idCard);}
    public HealthCardDetails getHealthCard(Integer idCard){return rcmServiceHelper.getHealthCard(idCard);}
    public Response saveEOBDetails(String jsonString){return rcmServiceHelper.saveEOBDetails(jsonString);}
    
    public String doPayment(String claimId){return rcmServiceHelper.doPayment(claimId);}
    //public String uploadEob(String jsonString) {return rcmServiceHelper.uploadEob(jsonString);}
    public List<EOBDetailsModel> getAllEOBDetails(){
        return rcmServiceHelper.getAllEOBDetails();
    }




}

