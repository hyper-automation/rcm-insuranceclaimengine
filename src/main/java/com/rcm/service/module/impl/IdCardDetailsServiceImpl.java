package com.rcm.service.module.impl;

import com.rcm.domain.SecurityDetails;
import com.rcm.entities.IdCardDetails;
import com.rcm.repositories.IdCardRepository;
import com.rcm.service.module.HealthCardDetailsService;
import com.rcm.service.module.IdCardDetailsService;
import com.rcm.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class IdCardDetailsServiceImpl implements IdCardDetailsService {

    @Autowired
    IdCardRepository idCardRepository;

    @Autowired
    HealthCardDetailsService healthCardDetailsService;

    @Override
    public void save(SecurityDetails securityDetails, Response response) {
        try {
            IdCardDetails idCardDetails = idCardRepository.save(securityDetails.getId_card());
            if (idCardDetails.getId_number().equalsIgnoreCase("D12345678")) {
                String pid = "PID1234";
                String superbillName = "Joe_Superbill";
                healthCardDetailsService.save(securityDetails, idCardDetails.getId(), pid, idCardDetails.getFirst_name(), superbillName, response);
            } else if (idCardDetails.getId_number().equalsIgnoreCase("98765438")) {
                String pid = "PID5438";
                String superbillName = "hapburn_superbill";
                healthCardDetailsService.save(securityDetails, idCardDetails.getId(), pid, idCardDetails.getFirst_name(), superbillName, response);
            } else if (idCardDetails.getId_number().equalsIgnoreCase("S514-172-80-844-0")) {
                String pid = "PID1417";
                String superbillName = "hapburn_superbill";
                healthCardDetailsService.save(securityDetails, idCardDetails.getId(), pid, idCardDetails.getFirst_name(), superbillName, response);
            } else if (idCardDetails.getId_number().equalsIgnoreCase("D09257418")) {
                String pid = "PID9257";
                String superbillName = "hapburn_superbill";
                healthCardDetailsService.save(securityDetails, idCardDetails.getId(), pid, idCardDetails.getFirst_name(), superbillName, response);
            } else if (idCardDetails.getId_number().equalsIgnoreCase("D08040785")) {
                String pid = "PID0785";
                String superbillName = "hapburn_superbill";
                healthCardDetailsService.save(securityDetails, idCardDetails.getId(), pid, idCardDetails.getFirst_name(), superbillName, response);
            } else if (idCardDetails.getId_number().equalsIgnoreCase("B09652845")) {
                String pid = "PID2845";
                String superbillName = "hapburn_superbill";
                healthCardDetailsService.save(securityDetails, idCardDetails.getId(), pid, idCardDetails.getFirst_name(), superbillName, response);
            } else if (idCardDetails.getId_number().equalsIgnoreCase("D10621461")) {
                String pid = "PID1461";
                String superbillName = "hapburn_superbill";
                healthCardDetailsService.save(securityDetails, idCardDetails.getId(), pid, idCardDetails.getFirst_name(), superbillName, response);
            } else if (idCardDetails.getId_number().equalsIgnoreCase("4817502")) {
                String pid = "PID7502";
                String superbillName = "hapburn_superbill";
                healthCardDetailsService.save(securityDetails, idCardDetails.getId(), pid, idCardDetails.getFirst_name(), superbillName, response);
            } else if (idCardDetails.getId_number().equalsIgnoreCase("1895663")) {
                String pid = "PID5663";
                String superbillName = "hapburn_superbill";
                healthCardDetailsService.save(securityDetails, idCardDetails.getId(), pid, idCardDetails.getFirst_name(), superbillName, response);
            } else if(idCardDetails.getId_number().equalsIgnoreCase("D400-7836-0001")) {
                String pid = "PID7836";
                String superbillName = "Walter_superbill";
                healthCardDetailsService.save(securityDetails, idCardDetails.getId(), pid, idCardDetails.getFirst_name(), superbillName, response);
            }else{
                String idnumber=idCardDetails.getId_number();
                String pid="PID"+idnumber.substring(1,5);
                String superbillName=idCardDetails.getFirst_name()+"_superbill";
                healthCardDetailsService.save(securityDetails, idCardDetails.getId(), pid, idCardDetails.getFirst_name(), superbillName, response);

            }
            response.setMessage("Details are saved");
            response.setStatus(200);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setMessage("Id Card Details not saved");
            response.setStatus(400);
        }
    }

    @Override
    public void getAllIdcards(List<SecurityDetails> securityDetailsList) {

        List<IdCardDetails> idCardDetailsList = idCardRepository.findAll();
        for (IdCardDetails idCardDetails : idCardDetailsList) {
            SecurityDetails securityDetails = new SecurityDetails();
            securityDetails.setId_card(idCardDetails);
            healthCardDetailsService.getByIdcard(securityDetails, idCardDetails.getId());
            securityDetailsList.add(securityDetails);
        }

    }

    @Override
    public IdCardDetails getIdCard(Integer idCard) {
        IdCardDetails idCardDetails = idCardRepository.findById(idCard).get();
        return idCardDetails;
    }
    
    @Override
    public List<IdCardDetails> getIdCardDetails(List<Integer> ids){
    	return idCardRepository.findByIdIn(ids);
    }
}
