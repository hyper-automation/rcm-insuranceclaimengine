package com.rcm.repositories;

import com.rcm.entities.IdCardDetails;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.*;

@Repository
public interface IdCardRepository extends JpaRepository<IdCardDetails, Integer> {
	List<IdCardDetails> findByIdIn(List<Integer> ids);
}
