package com.rcm.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Data
@Entity
@Table(name="IdCardDetails")
public class IdCardDetails implements Serializable{
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String card_type;
    @Column
    private String state;
    @Column
    private String country;
    @Column
    private String id_number;
    @Column
    private String first_name;
    @Column
    private String last_name;
    @Column
    private String dob;
    @Column
    private String gender;
    @Column
    private String weight;
    @Column
    private String height;
    @Column
    private String eye_color;
    @Column
    private String address;
    @Column
    private String superBillName;

}
