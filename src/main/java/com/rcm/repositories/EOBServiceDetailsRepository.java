package com.rcm.repositories;

import com.rcm.entities.EOBServiceDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EOBServiceDetailsRepository extends JpaRepository<EOBServiceDetails,Integer> {
    List<EOBServiceDetails> findByEobId(Integer eobId);
}
