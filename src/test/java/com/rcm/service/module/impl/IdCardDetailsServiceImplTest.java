package com.rcm.service.module.impl;

import com.rcm.RcmInsuranceClaimEngineApplicationTest;
import com.rcm.domain.SecurityDetails;
import com.rcm.entities.HealthCardDetails;
import com.rcm.entities.IdCardDetails;
import com.rcm.util.Response;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.Id;
import java.io.InvalidClassException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@SpringBootTest
class IdCardDetailsServiceImplTest extends RcmInsuranceClaimEngineApplicationTest {
    @Autowired
    IdCardDetailsServiceImpl idCardDetailsService;

    @Test
    void saveOne() {
        IdCardDetails idCardDetails=new IdCardDetails();
        Response response=new Response();
        HealthCardDetails healthCardDetails=new HealthCardDetails();
        setIdCardDetails(idCardDetails);
        setHealthCardDetails(healthCardDetails);
        idCardDetails.setId_number("D12345678");
        SecurityDetails securityDetails=new SecurityDetails();
        securityDetails.setId_card(idCardDetails);
        securityDetails.setHealth_card(healthCardDetails);
        idCardDetailsService.save(securityDetails,response);
        Assert.assertEquals("Details are saved",response.getMessage());


    }
    @Test
    void saveTwo() {
        IdCardDetails idCardDetails=new IdCardDetails();
        Response response=new Response();
        HealthCardDetails healthCardDetails=new HealthCardDetails();
        setIdCardDetails(idCardDetails);
        setHealthCardDetails(healthCardDetails);
        idCardDetails.setId_number("98765438");
        SecurityDetails securityDetails=new SecurityDetails();
        securityDetails.setId_card(idCardDetails);
        securityDetails.setHealth_card(healthCardDetails);
        idCardDetailsService.save(securityDetails,response);
        Assert.assertEquals("Details are saved",response.getMessage());

    }
    @Test
    void saveThree() {
        IdCardDetails idCardDetails=new IdCardDetails();
        Response response=new Response();
        HealthCardDetails healthCardDetails=new HealthCardDetails();
        setIdCardDetails(idCardDetails);
        setHealthCardDetails(healthCardDetails);
        idCardDetails.setId_number("S514-172-80-844-0");
        SecurityDetails securityDetails=new SecurityDetails();
        securityDetails.setId_card(idCardDetails);
        securityDetails.setHealth_card(healthCardDetails);
        idCardDetailsService.save(securityDetails,response);
        Assert.assertEquals("Details are saved",response.getMessage());

    }
    @Test
    void saveFour() {
        IdCardDetails idCardDetails=new IdCardDetails();
        Response response=new Response();
        HealthCardDetails healthCardDetails=new HealthCardDetails();
        setIdCardDetails(idCardDetails);
        setHealthCardDetails(healthCardDetails);
        idCardDetails.setId_number("D09257418");
        SecurityDetails securityDetails=new SecurityDetails();
        securityDetails.setId_card(idCardDetails);
        securityDetails.setHealth_card(healthCardDetails);
        idCardDetailsService.save(securityDetails,response);
        Assert.assertEquals("Details are saved",response.getMessage());

    }
    @Test
    void saveFive() {
        IdCardDetails idCardDetails=new IdCardDetails();
        Response response=new Response();
        HealthCardDetails healthCardDetails=new HealthCardDetails();
        setIdCardDetails(idCardDetails);
        setHealthCardDetails(healthCardDetails);
        idCardDetails.setId_number("D08040785");
        SecurityDetails securityDetails=new SecurityDetails();
        securityDetails.setId_card(idCardDetails);
        securityDetails.setHealth_card(healthCardDetails);
        idCardDetailsService.save(securityDetails,response);
        Assert.assertEquals("Details are saved",response.getMessage());

    }
    @Test
    void saveSix() {
        IdCardDetails idCardDetails=new IdCardDetails();
        Response response=new Response();
        HealthCardDetails healthCardDetails=new HealthCardDetails();
        setIdCardDetails(idCardDetails);
        setHealthCardDetails(healthCardDetails);
        idCardDetails.setId_number("B09652845");
        SecurityDetails securityDetails=new SecurityDetails();
        securityDetails.setId_card(idCardDetails);
        securityDetails.setHealth_card(healthCardDetails);
        idCardDetailsService.save(securityDetails,response);
        Assert.assertEquals("Details are saved",response.getMessage());

    }
    @Test
    void saveSeven() {
        IdCardDetails idCardDetails=new IdCardDetails();
        Response response=new Response();
        HealthCardDetails healthCardDetails=new HealthCardDetails();
        setIdCardDetails(idCardDetails);
        setHealthCardDetails(healthCardDetails);
        idCardDetails.setId_number("D10621461");
        SecurityDetails securityDetails=new SecurityDetails();
        securityDetails.setId_card(idCardDetails);
        securityDetails.setHealth_card(healthCardDetails);
        idCardDetailsService.save(securityDetails,response);
        Assert.assertEquals("Details are saved",response.getMessage());

    }
    @Test
    void saveEight() {
        IdCardDetails idCardDetails=new IdCardDetails();
        Response response=new Response();
        HealthCardDetails healthCardDetails=new HealthCardDetails();
        setIdCardDetails(idCardDetails);
        setHealthCardDetails(healthCardDetails);
        idCardDetails.setId_number("4817502");
        SecurityDetails securityDetails=new SecurityDetails();
        securityDetails.setId_card(idCardDetails);
        securityDetails.setHealth_card(healthCardDetails);
        idCardDetailsService.save(securityDetails,response);
        Assert.assertEquals("Details are saved",response.getMessage());
    }
    @Test
    void saveNine() {
        IdCardDetails idCardDetails=new IdCardDetails();
        Response response=new Response();
        HealthCardDetails healthCardDetails=new HealthCardDetails();
        setIdCardDetails(idCardDetails);
        setHealthCardDetails(healthCardDetails);
        idCardDetails.setId_number("1895663");
        SecurityDetails securityDetails=new SecurityDetails();
        securityDetails.setId_card(idCardDetails);
        securityDetails.setHealth_card(healthCardDetails);
        idCardDetailsService.save(securityDetails,response);
        Assert.assertEquals("Details are saved",response.getMessage());

    }
    @Test
    void saveTen() {
        IdCardDetails idCardDetails=new IdCardDetails();
        Response response=new Response();
        HealthCardDetails healthCardDetails=new HealthCardDetails();
        setIdCardDetails(idCardDetails);
        setHealthCardDetails(healthCardDetails);
        idCardDetails.setId_number("1234567");
        SecurityDetails securityDetails=new SecurityDetails();
        securityDetails.setId_card(idCardDetails);
        securityDetails.setHealth_card(healthCardDetails);
        idCardDetailsService.save(securityDetails,response);
        Assert.assertEquals("Details are saved",response.getMessage());

    }
    @Test
    void save() {
        IdCardDetails idCardDetails=new IdCardDetails();
        Response response=new Response();
        setIdCardDetails(idCardDetails);
        idCardDetailsService.save(new SecurityDetails(),response);
        Assert.assertEquals("Id Card Details not saved",response.getMessage());

    }

    @Test
    void getAllIdcards() {
            List<SecurityDetails> securityDetailsList = new ArrayList<SecurityDetails>();
            idCardDetailsService.getAllIdcards(securityDetailsList);
            Assert.assertEquals(true,securityDetailsList.size()>0);

    }
    private void setIdCardDetails(IdCardDetails idCardDetails){
        idCardDetails.setCard_type("driving");
        idCardDetails.setAddress("USA");
        idCardDetails.setState("NZ");
        idCardDetails.setCountry("USA");
        idCardDetails.setDob("07/07/94");
        idCardDetails.setFirst_name("Smith James");
        idCardDetails.setLast_name("James");
        idCardDetails.setGender("male");
        idCardDetails.setWeight("80");
        idCardDetails.setId_number("D12345678");
        idCardDetails.setHeight("5.7");
        idCardDetails.setEye_color("blue");
        idCardDetails.setSuperBillName("walter");
    }
    private void setHealthCardDetails(HealthCardDetails healthCardDetails){
        healthCardDetails.setInsurance_provider("Anthem");
        healthCardDetails.setInsurance_plan("BPPO");
        healthCardDetails.setFirst_name("Smith James");
        healthCardDetails.setLast_name("James");
        healthCardDetails.setId_number("11111111");
        healthCardDetails.setRxBIN("58477");
        healthCardDetails.setGroup_id("HRNYBT");
        healthCardDetails.setCard_issued_date(new Date("1/1/21"));
    }
}