package com.rcm.domain;

import com.rcm.entities.EOBServiceDetails;

import com.rcm.entities.HealthCardDetails;
import com.rcm.entities.PatientDetails;
import com.rcm.entities.IdCardDetails;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
@Getter
@Setter
public class EOBDetailsModel {
    private Integer eobId;

    private String patientId;

    private String claimId;

    private String superBillId;

    private String  providerId;

    private String patientName;
    
    private String eobFilePath;

    private List<EOBServiceDetails> EOBServiceDetails;
    
    private List<HealthCardDetails> HealthCardDetails;
    
    private List<PatientDetails> PatientDetails;
    
    private List<IdCardDetails> IdCardDetails;
}
