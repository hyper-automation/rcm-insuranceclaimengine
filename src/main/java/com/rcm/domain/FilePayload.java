package com.rcm.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class FilePayload {
    private String file_type;
    private String base64;
}
