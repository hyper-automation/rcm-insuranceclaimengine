package com.rcm.service;

import com.rcm.domain.EOBDetailsModel;
import org.json.JSONObject;
import com.rcm.domain.SecurityDetails;
import com.rcm.entities.*;
import com.rcm.service.module.EOBDetailsService;
import com.rcm.service.module.HealthCardDetailsService;
import com.rcm.service.module.IdCardDetailsService;
import com.rcm.service.module.PatientDetailsService;
import com.rcm.util.ClaimResponse;
import com.rcm.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class RcmServiceHelper {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    IdCardDetailsService idCardDetailsService;

    @Autowired
    PatientDetailsService patientDetailsService;
    @Autowired
    HealthCardDetailsService healthCardDetailsService;
    @Autowired
    EOBDetailsService eobDetailsService;
/*
    public SecurityDetails processTwoCards(RequestPayload requestPayload) {
        String url = "http://20.198.72.26:5003/extract_info_two_cards";
        HttpEntity request = new HttpEntity(requestPayload, new HttpHeaders());
        log.debug("prediction started");
        log.info("prediction started");
        ResponseEntity<SecurityDetails> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, SecurityDetails.class);
        SecurityDetails securityDetails = new SecurityDetails();
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            securityDetails = responseEntity.getBody();
        }
        log.info("prediction is ended");
        log.debug("prediction is ended");
        return securityDetails;
    }

    public Object processOneCard(FilePayload filePayload) {
        String url = "http://20.198.72.26:5003/extract_info_single_card";
        log.debug("prediction started");
        log.info("prediction started");
        HttpEntity request = new HttpEntity(filePayload, new HttpHeaders());
        ResponseEntity<Object> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);
        log.info("prediction is ended");
        log.debug("prediction is ended");
        return responseEntity.getBody();

    }*/

    public Response saveDetails(SecurityDetails securityDetails) {
        Response response = new Response();
        idCardDetailsService.save(securityDetails, response);
        return response;
    }

    public List<SecurityDetails> getAll() {
        List<SecurityDetails> securityDetailsList = new ArrayList<>();
        idCardDetailsService.getAllIdcards(securityDetailsList);
        return securityDetailsList;
    }

    public List<PatientDetails> getAllPatients() {
        List<PatientDetails> patientDetailsList = new ArrayList<>();
        patientDetailsService.getAllPatients(patientDetailsList);
        return patientDetailsList;
    }

    public String updatePatientDetails(String jsonString) {
        Response response = new Response();
        patientDetailsService.update(jsonString, response);
        return response.getMessage();
    }

    public String verify(String jsonString) {
        Response response = new Response();
        patientDetailsService.updateVerify(jsonString, response);
        return response.getMessage();
    }

    public ClaimResponse createClaim(String jsonString) {
        ClaimResponse response = new ClaimResponse();
        patientDetailsService.createClaim(response, jsonString);
        return response;
    }

    public IdCardDetails getIdCard(Integer idCard) {
        return idCardDetailsService.getIdCard(idCard);
    }

    public HealthCardDetails getHealthCard(Integer idCard) {
        return healthCardDetailsService.getHealthCard(idCard);
    }
    public Response saveEOBDetails(String jsonString){
        Response response= new Response();
        eobDetailsService.save(jsonString,response);
        return response;
    }
    public List<EOBDetailsModel> getAllEOBDetails() {
        List<EOBDetailsModel> eobDetailsModelList = new ArrayList<>();
        eobDetailsService.getAllEOBDetails(eobDetailsModelList);
        return eobDetailsModelList;
    }
    
    public String doPayment(String claimId) {
        Response response = new Response();
        patientDetailsService.updatePayStatus(claimId, response);
        return response.getMessage();
    }
    
    /*public String uploadEob(String jsonString) {
    	Response response = new Response();
    	JSONObject jsonObject = new JSONObject(jsonString);
    	String patientId = jsonObject.getString("patientId");
    	String filename = jsonObject.getString("filename");
    	patientDetailsService.updatefilePath(filename,patientId,response);
    	return response.getMessage();
    }*/
}
