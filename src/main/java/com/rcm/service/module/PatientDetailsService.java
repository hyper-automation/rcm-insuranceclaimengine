package com.rcm.service.module;



import com.rcm.entities.PatientDetails;

import com.rcm.util.ClaimResponse;
import com.rcm.util.Response;

import java.util.List;

public interface PatientDetailsService {
    void save(Integer idCard ,String pid,String name,String superbillName, Response response,String insurenceNumber);
    void update(String string,Response response);
    void updateVerify(String string, Response response);
    void getAllPatients(List<PatientDetails> patientDetailsList);
    void createClaim(ClaimResponse response, String jsonString);
    void updatePayStatus(String claimId,Response response);
    //String updatefilePath(String filename, String patientId,Response response);
    List<PatientDetails> getPatientDetails(String patientId);
}
