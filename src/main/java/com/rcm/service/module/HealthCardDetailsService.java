package com.rcm.service.module;

import com.rcm.domain.SecurityDetails;

import com.rcm.entities.HealthCardDetails;
import com.rcm.util.Response;
import java.util.*;

public interface HealthCardDetailsService {
    void save(SecurityDetails securityDetails,Integer idCard,String pid,String name, String superbillname, Response response);
    void getByIdcard(SecurityDetails securityDetails,Integer id);
    HealthCardDetails getHealthCard(Integer idCard);
}
